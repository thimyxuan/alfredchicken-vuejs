import Vue from 'vue'
import Router from 'vue-router'
import Blog from '@/components/Blog'
import Projects from '@/components/Projects'
import ProjectDetails from '@/components/ProjectDetails'
import ProjectsFilter from '@/components/ProjectsFilter'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homepage',
      component: Projects
    },
    {
      path: '/project-details/:pid',
      name: 'project-details',
      component: ProjectDetails
    },
    {
      path: '/projects-filter&:filter',
      name: 'projects-filter',
      component: ProjectsFilter
    },
    {
      path: '/blog',
      name: 'blog',
      component: Blog
    }
  ]
})
